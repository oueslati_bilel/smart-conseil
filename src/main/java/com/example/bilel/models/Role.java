package com.example.bilel.models;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private com.example.bilel.models.ERole name;

	public Role() {

	}

	public Role(com.example.bilel.models.ERole name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public com.example.bilel.models.ERole getName() {
		return name;
	}

	public void setName(com.example.bilel.models.ERole name) {
		this.name = name;
	}
}