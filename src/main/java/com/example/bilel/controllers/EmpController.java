package com.example.bilel.controllers;

import com.example.bilel.models.Employee;
import com.example.bilel.repository.EmpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200", maxAge= 3600)
@RestController
@RequestMapping("/api")
public class EmpController {
    @Autowired
    EmpRepository empRepository;


    @GetMapping("/employee")
    public ResponseEntity<List<Employee>> getAllTutorials(@RequestParam(required = false) String adress) {
        try {
            List<Employee> employees = new ArrayList<Employee>();

            if (adress == null)
                empRepository.findAll().forEach(employees::add);
            else
                empRepository.findByAdress(adress).forEach(employees::add);

            if (employees.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(employees, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getTutorialById(@PathVariable("id") long id) {
        Optional<Employee> emplData = empRepository.findById(id);

        if (emplData.isPresent()) {
            return new ResponseEntity<>(emplData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/addEmp")
    public ResponseEntity<Employee> createTutorial(@RequestBody Employee employee) {
        try {
            Employee empl = empRepository
                    .save(new Employee(employee.getId(),employee.getNom(), employee.getPrenom(),employee.getAdress()));
            return new ResponseEntity<>(empl, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
        try {
            empRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Employee> updateTutorial(@PathVariable("id") long id, @RequestBody Employee employee) {
        Optional<Employee> empData = empRepository.findById(id);

        if (empData.isPresent()) {
            Employee employee1 = empData.get();
            employee1.setNom(employee.getNom());
            employee1.setPrenom(employee.getPrenom());
            employee1.setAdress(employee.getAdress());
            return new ResponseEntity<>(empRepository.save(employee1), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
