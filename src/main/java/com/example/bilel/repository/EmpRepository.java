package com.example.bilel.repository;

import com.example.bilel.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EmpRepository extends JpaRepository<Employee,Long> {
    List<Employee> findByNom(String nom);

    List<Employee> findByAdress(String adress);
}
