package com.example.bilel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BilelApplication {

	public static void main(String[] args) {
		SpringApplication.run(BilelApplication.class, args);
	}

}
